HTML Corrector Corrector

The HTML Corrector in Drupal 6 incorrectly escapes HTML comments, 
so they will be visible on the site. 

Still not fixed for 6.14 at the time of this writing, 
so I'm releasing this very simple filter to put after the HTML corrector filter
that will fix the bug. This module will be discontinued once the bug is fixed.

Installation:

* Enable the module
* Edit your desired input format, enable the htmlcorrector and the htmlcorrectorcorrector filters
* Rearrange the filters so that HTML corrector corrector is below HTML corrector
